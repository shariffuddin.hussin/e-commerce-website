<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();
        

        $adminRole = Role::where('name', 'admin')->first();
        $sellerRole = Role::where('name', 'seller')->first();
        $customerRole = Role::where('name', 'customer')->first();

        $admin = User::create ([
            'name' => 'admin',
            'email' => 'master@gmail.com',
            'password' => Hash::make('master1234')
        ]);

        $seller = User::create ([
            'name' => 'seller',
            'email' => 'seller@gmail.com',
            'password' => Hash::make('seller1234')
        ]);

        $customer = User::create ([
            'name' => 'Roti',
            'email' => 'Roti@gmail.com',
            'password' => Hash::make('roti1234')
        ]);

        //attach method

        $admin->roles()->attach($adminRole);
        $seller->roles()->attach($sellerRole);
        $customer->roles()->attach($customerRole);

    }
}
